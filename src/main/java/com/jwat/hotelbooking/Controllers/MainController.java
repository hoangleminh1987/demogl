package com.jwat.hotelbooking.Controllers;

import java.util.List;

import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jwat.hotelbooking.Models.Guest;
import com.jwat.hotelbooking.Models.RoomReserved;
import com.jwat.hotelbooking.Services.CategoryServices;
import com.jwat.hotelbooking.Services.GuestServices;
import com.jwat.hotelbooking.Services.RoomReservedServices;
import com.jwat.hotelbooking.Services.RoomServices;
import com.jwat.hotelbooking.Services.TypeServices;

@Controller
@Transactional
public class MainController {
    @Autowired
    private RoomServices roomServices;  
    @Autowired
    private RoomReservedServices roomReserved;
    @Autowired
    private TypeServices typeServices;
    @Autowired
    private GuestServices guestServices;
    

    @RequestMapping("/getAllGuest")
    public List<Guest> getAllGuest(Model model) {
        List<Guest> guests = guestServices.getAllGuests();
        model.addAttribute("guests", guests);
        return guests;
    }
    


}
