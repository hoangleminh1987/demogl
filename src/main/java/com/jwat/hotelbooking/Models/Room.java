package com.jwat.hotelbooking.Models;

import javax.persistence.*;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "room")
@Getter
@Setter
@ConstructorBinding
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name ="roomName", nullable = false, length = 50)
    private String roomName;

    @Column(name ="roomPrice", nullable = false, length = 50)
    private double roomPrice;

    @Column(name ="roomDescription", nullable = true, length = 50)
    private String roomDescription;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "typeId")
    private Type type;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "hotelId")
    private Hotel hotel;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
    private java.util.List<RoomReserved> roomReserveds;
}