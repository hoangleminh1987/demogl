package com.jwat.hotelbooking.Models;

import javax.persistence.*;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "type")
@Getter
@Setter
@ConstructorBinding
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String typeName;

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL)
    private java.util.List<Room> rooms;
}
