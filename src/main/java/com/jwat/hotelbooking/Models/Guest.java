package com.jwat.hotelbooking.Models;

import javax.persistence.*;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "guest")
@Getter
@Setter
@ConstructorBinding
public class Guest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String guestFirstName;

    @Column(nullable = false, length = 50)
    private String guestLastName;

    @Column(nullable = false, length = 50)
    private String guestEmail;

    @Column(nullable = false, length = 50)
    private String guestPhone;

    @Column(nullable = false, length = 50)
    private String guestAddress;

    @Column(nullable = false, length = 50)
    private boolean guestGender;

    @Column(nullable = false, length = 50)
    private String guestNote;
    
    

    @OneToMany(mappedBy = "guest", cascade = CascadeType.ALL)
    private java.util.List<Invoice> invoices;

    @OneToMany(mappedBy = "guest", cascade = CascadeType.ALL)
    private java.util.List<Reservation> reservations;
}
