package com.jwat.hotelbooking.Models;

import javax.persistence.*;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "roomReserved")
@Getter
@Setter
@ConstructorBinding
public class RoomReserved {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name ="totalPrice", nullable = false, length = 50)
    private double totalPrice;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "roomId")
    private Room room;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "reservationId")
    private Reservation reservation;

}
