package com.jwat.hotelbooking.Models;

import java.util.Date;

import javax.persistence.*;

import org.springframework.boot.context.properties.ConstructorBinding;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "invoice")
@Getter
@Setter
@ConstructorBinding
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String invoiceNumber;

    @Basic(optional = false)
    @Column(name = "tsIssued", nullable = false, length = 50)
    @Temporal(TemporalType.DATE)
    private Date tsIssued;

    @Basic(optional = false)
    @Column(name = "tsPaid", nullable = true, length = 50)
    @Temporal(TemporalType.DATE)
    private Date tsPaid;

    @Basic(optional = false)
    @Column(name = "tsCanceled", nullable = true, length = 50)
    @Temporal(TemporalType.DATE)
    private Date tsCanceled;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "guestId")
    private Guest guest;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "reservationId")
    private Reservation reservation;


}
