package com.jwat.hotelbooking.Repositories;

import org.springframework.data.repository.CrudRepository;

import com.jwat.hotelbooking.Models.Guest;

public interface GuestRepository extends CrudRepository<Guest, Long> {

    public Long countById(Long id);

    public void deleteByGuestFirstName(String string);

}
