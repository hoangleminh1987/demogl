package com.jwat.hotelbooking.Repositories;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;

import com.jwat.hotelbooking.Models.Reservation;

public interface ReservationRepository extends CrudRepository<Reservation, Long> {

    public Long countById(Long id);

    public Long countByTsReserved(Date tsReserved);

}
