package com.jwat.hotelbooking.Repositories;

import org.springframework.data.repository.CrudRepository;

import com.jwat.hotelbooking.Models.Hotel;

public interface HotelRepository extends CrudRepository<Hotel, Long> {

    public Long countById(Long id);

}
