package com.jwat.hotelbooking.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jwat.hotelbooking.Models.RoomReserved;
import com.jwat.hotelbooking.Repositories.RoomReservedRepository;

@Service
public class RoomReservedServices {
    @Autowired
    private RoomReservedRepository roomReservedRepository;

    // list all
    public List<RoomReserved> getAllRoomReserved() {
        return (List<RoomReserved>) roomReservedRepository.findAll();
    }

    // find by id
    public RoomReserved getRoomReservedById(Long id) throws NotFoundException {
        Optional<RoomReserved> result = roomReservedRepository.findById(id);
        if (result.isPresent()) {
            return result.get();
        } else {
            throw new NotFoundException("RoomReserved not found");
        }
    }

    //getTotalRoomReserved
    public int getTotalRoomReserved() {
        Long total = roomReservedRepository.count();
        return total.intValue();
    }

    // save
    public RoomReserved save(RoomReserved roomReserved) {
        return roomReservedRepository.save(roomReserved);
    }

    // delete
    public void delete(Long id) throws NotFoundException {
        Long count = roomReservedRepository.countById(id);
        if (count == 0) {
            throw new NotFoundException("RoomReserved not found");
        } else {
            roomReservedRepository.deleteById(id);
        }
    }

    public Long countTotalRoomsReserved() {
        return roomReservedRepository.count();
    }
}
